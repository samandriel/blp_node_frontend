import express from 'express'
import morgan from 'morgan'
import nunjucks from 'nunjucks'
import path from 'path'
import routes from './routes.js'
// import nocache from 'nocache'

const app = express()

// Configs
nunjucks.configure(path.resolve(__dirname, 'views'), {
  autoescape: true,
  noCache: true,
  express: app
})

// Middlewares
app.use(morgan('dev'))
// app.use(nocache())
app.use(express.static(path.resolve(__dirname, 'assets')))
app.use('/', routes)

// Start server
const port = process.env.PORT || 8000
app.listen(port, () => {
  console.log(`Server running on port ${port}.`)
})

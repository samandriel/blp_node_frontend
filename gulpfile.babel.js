import gulp from 'gulp'
import browserSync from 'browser-sync'
import sourcemaps from 'gulp-sourcemaps'
// import gulpCopy from 'gulp-copy'
import nodemon from 'nodemon'
// import clean from 'gulp-clean'
import concat from 'gulp-concat'
import babel from 'gulp-babel'
// import nunjucksRender from 'gulp-nunjucks-render'
import composer from 'gulp-uglify/composer'
import uglifyES from 'uglify-es'
import imagemin from 'gulp-imagemin'
import pump from 'pump'
import stylus from 'gulp-stylus'
import nib from 'nib'
import minifyCSS from 'gulp-clean-css'

const minifyJS = composer(uglifyES, console)

// Compile Nunjucks
gulp.task('html', (cb) => {
  pump([
    gulp.src('src/views/**/*.html'),
    gulp.dest('dist/views')
  ],
  cb)
})

// // Compile ES6
// gulp.task('app', (cb) => {
//   pump([
//     gulp.src(['src/app.js']),
//     sourcemaps.init(),
//     babel(),
//     sourcemaps.write('.'),
//     gulp.dest('dist')
//   ],
//   cb)
// })

// Compile ES6 Backend
gulp.task('js', (cb) => {
  pump([
    gulp.src(['src/**/*.js', '!src/assets/js/**/*.js']),
    sourcemaps.init(),
    babel(),
    sourcemaps.write('.'),
    gulp.dest('dist')
  ],
  cb)
})

// Compile ES6 Asset
gulp.task('jsAssets', (cb) => {
  pump([
    gulp.src('src/assets/js/**/*.js'),
    sourcemaps.init(),
    babel(),
    concat('app.min.js'),
    minifyJS(),
    sourcemaps.write('.'),
    gulp.dest('dist/assets/js')
  ],
  cb)
})

// Compile Stylus
gulp.task('stylus', (cb) => {
  pump([
    gulp.src('src/assets/stylus/**/*.styl'),
    sourcemaps.init(),
    stylus({
      compress: true,
      use: nib()
    }),
    minifyCSS(),
    sourcemaps.write(),
    gulp.dest('dist/assets/css')
  ],
  cb)
})

// Minify Image
gulp.task('images', function (cb) {
  pump([
    gulp.src('src/assets/images/**/*'),
    imagemin(),
    gulp.dest('dist/assets/images')
  ],
  cb)
})

// Wait all tasks to finish running in order to reload page
// gulp.task('app-watch', ['app'], function (done) {
//   browserSync.reload()
//   done()
// })
gulp.task('js-watch', ['js'], function (done) {
  browserSync.reload()
  done()
})
gulp.task('jsAssets-watch', ['jsAssets'], function (done) {
  browserSync.reload()
  done()
})
gulp.task('stylus-watch', ['stylus'], function (done) {
  browserSync.reload()
  done()
})
gulp.task('html-watch', ['html'], function (done) {
  browserSync.reload()
  done()
})
gulp.task('images-watch', ['images'], function (done) {
  browserSync.reload()
  done()
})

//  ========================
// Browsersync
// gulp.task('watch', () => {
//   browserSync.init({
//     server: {
//       proxy: 'http://localhost:8000'
//     }
//   })

//   gulp.watch('src/assets/js/**/*.js', ['js-watch'])
//   gulp.watch('src/assets/images/**/*', ['images-watch'])
//   gulp.watch('src/assets/stylus/**/*.styl', ['stylus-watch'])
//   gulp.watch('src/**/*.html', ['html-watch'])
// })

// Default
gulp.task('default', ['js', 'jsAssets', 'stylus', 'html', 'images'])

// clean
// gulp.task('clean', (done) => {
//   pump([
//     gulp.src('dist', {read: false}),
//     clean()
//   ])
//   done()
// })

// nodemon
gulp.task('dev', ['default'], () => {
  nodemon({
    script: 'dist/app.js'
  }).on('start', () => {
    browserSync.init({
      proxy: 'localhost:8000'
    })
  })

  // gulp.watch('src/**/*.js', ['app-watch'])
  gulp.watch('src/**/*.js', ['js-watch'])
  gulp.watch('src/assets/js/**/*.js', ['jsAssets-watch'])
  gulp.watch('src/assets/images/**/*', ['images-watch'])
  gulp.watch('src/assets/stylus/**/*.styl', ['stylus-watch'])
  gulp.watch('src/views/**/*.html', ['html-watch'])
})
